import {db} from '@/firebase/index'
import * as firebase from "firebase";
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;
import Type from "@/components/portal/Type.vue";
import {store} from "@/store/store";

/*

Đây là base Model, chúng ta sẽ viết những methods thường xuyên sử dụng cho các Models, ví dụ:

- insert
- update
- delete
- getById
- query...

 */

export default class Model {
    static COLLECTION: string;
    COLLECTION: string;
    firestore: firebase.firestore.Firestore;
    id: string = "";
    created_date: number = Date.now();
    updated_date: number = Date.now();
    created_by: string = "";
    updated_by: string = "";
    createdAt: number = Date.now();
    updatedAt: number = Date.now();


    static _onAdded: Function;
    static _onUpdated: Function;
    static _onDeleted: Function;

    // create new instance, not single instance, instead of (new Model())
    static get instance(): Model {
        throw new TypeError('override this');
    }

    static from(data: any = {}, id?: string) {
        const newObj = this.instance;
        const obj = typeof data === 'object' ? data : {};
        if (!id && !data.id) {
            throw new Error("id not found");
        }

        Object.assign(newObj, {id}, obj, {id});

        return newObj;
    }

    constructor(COLLECTION: string) {
        this.firestore = db;
        this.COLLECTION = COLLECTION;
        this.constructor['COLLECTION'] = COLLECTION;
    }


    deserialize(data: Object) {
        Object.assign(this, data);
    }

    async insert(): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let result = {...this};
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.created_date = Date.now(); //TIMESTAMP;
        result.updated_date = Date.now(); //TIMESTAMP;

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.createdAt = Date.now(); //TIMESTAMP;
        result.updatedAt = Date.now(); //TIMESTAMP;

        const {store} = require('@/store/store');
        result.updated_by =  store.getters.currentUser.id;
        result.created_by = store.getters.currentUser.id;


        const model = await modelCollection.add(JSON.parse(JSON.stringify(result)));
        this.id = model.id;

        return this;
    }

    async upsert(): Promise<Model> {
        if (this.id) {
            return await this.update();
        } else {
            return await this.insert();
        }
    }

    async update(): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = Date.now();
        result.updatedAt = Date.now(); //TIMESTAMP;

        const {store} = require('@/store/store');
        result.updated_by =  store.getters.currentUser.id;


        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        return this;
    }

    async delete(): Promise<void> {

        const modelCollection = this.firestore.collection(this.COLLECTION);
        const record = await modelCollection.doc(this.id);

        return record.delete();
    }

    public collection(): firebase.firestore.CollectionReference {
        return db.collection(this.COLLECTION);
    }


    public async getById(id: string): Promise<Model> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        const record = await modelCollection.doc(id).get();
        if (!record.exists) {
            throw new Error(`${this.COLLECTION} ${id} not found`);
        }

        this.id = record.id;
        Object.assign(this, record.data());
        return this;
    }

    static listenRealtime(): void {
        this.instance.collection()
            .onSnapshot((snapshots) => {
                snapshots.docChanges().forEach((change) => {
                    const model = this.from(change.doc.data(), change.doc.id);
                    switch (change.type) {
                        case "added":
                            this._onAdded(model);
                            break;
                        case "modified":
                            this._onUpdated(model);
                            break;
                        case "removed":
                            this._onDeleted(model);
                        default:
                            break;
                    }
                });
            });
    }

    static onUpdated(callback: Function) {
        this._onUpdated = callback;
    }
    static onAdded(callback: Function) {
        this._onAdded = callback;
    }
    static onDeleted(callback: Function) {
        this._onDeleted = callback;
    }
    cloneNewObject(){
        // TODO: this function --> clone một object mới hoàn toàn không còn vùng nhớ ở object cữ
        let newObject={}
        let result = {...this};
        delete result['firestore'];// xóa vì khi parse JSON báo lỗi
        newObject = JSON.parse(JSON.stringify(result))
        return newObject
    }
}
