import Model from './base';
import firebase from "@/firebase";
import Company from "@/models/Company";
// import Router from '../router';
import {store} from "@/store/store";
import Team from "@/models/Team";



export default class User extends Model {
    name: string = "";
    company_id = "";
    photoURL: string = "";
    role:string ="1";
    photoName: string = "";
    signatureFlag: boolean = true;
    signatureHtml: string = "";
    meetingRemind:boolean = true;
    candidateAssign: boolean = true;
    taskAssign:  boolean = true;
    taskDuel:  boolean = true;
    status: string="1";
    candidate_star=[];

    constructor() {
        super("users");
    }

    static get instance(): User {
        return new User();
    }

    public async getCompany(): Promise<Company> {
        return Company.instance.getById(this.company_id) as Promise<Company>;
    }

    // public async getTeams(): Promise<Team[]> {
    //     const result = [] as Team[];
    //     for (const teamRef of this.teams) {
    //         const response = await teamRef.get();
    //         const team = Team.from(response.data()) as Team;
    //         team.id = teamRef.id;
    //         result.push(team);
    //     }
    //     return result;
    // }

    public async getCurrentUser(): Promise<User> {
        return store.getters.currentUser;
    }


    public async getAllUsers(company_id: string): Promise<User[]> {
        const results: User[] = [];

        const records = await this.collection()
            .where("company_id", "==", company_id)
            .limit(20)
            .get();

        records.forEach(docSnapshot => {
            let record = User.from(docSnapshot.data(), docSnapshot.id) as User;
            if (record.status != "1")
                results.push(record);
        });
        return results
    }

    public async getUserbyEmail(email: string): Promise<User> {
        let result: User = null;

        const records = await this.collection()
            .where("email", "==", email)
            .limit(1)
            .get();

        records.forEach(docSnapshot => {
            let record = User.from(docSnapshot.data(), docSnapshot.id) as User;
            result = record;
        });
        return result
    }

    public async getAllUsersNotMe(company_id: string, user_id: string): Promise<User[]> {
        const results: User[] = [];

        const records = await this.collection()
            .where("company_id", "==", company_id)
            .get();

        records.forEach(docSnapshot => {
            let record = User.from(docSnapshot.data(), docSnapshot.id) as User;
            if  (record.id != user_id)
                results.push(record);
        });
        return results;
    }


    // override

    async update(): Promise<User> {

        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = Date.now();
        result.updated_by =  store.getters.currentUser.id;

        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        return this;
    }

    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }
}
