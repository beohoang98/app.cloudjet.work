import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import User from "@/models/User";

export default class Question extends Model {
    questionnaire_id: string = "";
    response_type: null;
    name: string = '';
    description: string = '';
    options = [];
    required: boolean=false;
    constructor() {
        super("question");
    }
    public async getAllQuestion(questionnaire_id:string): Promise<Question[]> {
        const results: Question[] = new Array();
        let company = store.getters.currentCompany;

        if (company) {

            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("questionnaire_id", "==", questionnaire_id)
                .get();
            records.forEach(docSnapshot => {

                let record = new Question();

                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);

            });
        }
        return results
    }
}
