import Model from './base';
import {store} from "@/store/store";

export default class OtherInformation extends Model {

    title: string = "";
    value: string = "";
    company_id: string = "";
    is_encrypt: boolean = false;

    constructor() {
        super("OtherInformation");
    }

    public async getAllOtherInformation(): Promise<OtherInformation[]> {
        const results: OtherInformation[] = new Array();
        let company = store.getters.currentCompany;

        if (company) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("company_id", "==", company.id)
                .orderBy("createdAt", "desc")
                .limit(20)
                .get();
            records.forEach(docSnapshot => {
                let record = new OtherInformation();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);
            });
        }

        return results
    }


}
