import Model from './base';
import PipelineStage from "@/models/PipelineStage";
import {messages} from "@/messages";
import {store} from "@/store/store";

export default class Pipeline extends Model {

    title: string = "";
    company_id: string = "";
    is_default: boolean = false;
    used_positions: Array<object> = [];

    public deleteAllStateByID(position_stage_id: string) {
        if (position_stage_id) {
            const modelCollection = this.firestore.collection('PipelineStage');
            modelCollection.where("pipeline_id", "==", position_stage_id).get().then(function (querySnapshot) {
                querySnapshot.forEach(docSnapshot => {
                    docSnapshot.ref.delete();
                });
            })
        }
    }

    public async createPipelineDefault(company_id: string): Promise<Pipeline> {
        let defaultPineLine = new Pipeline();
        defaultPineLine.title = messages.SR_007_DEFAULT_PIPELINE_TITLE;
        defaultPineLine.company_id = company_id;
        defaultPineLine.is_default = true;
        defaultPineLine.insert().then((pipeline) => {
            new PipelineStage().getAllStages(pipeline.id);
            Object.assign(defaultPineLine, pipeline);
        });
        return defaultPineLine;
    }

    public async getAllPipelines(company_id: string): Promise<Pipeline[]> {
        const results: Pipeline[] = new Array();
        let that = this;
        if (company_id) {
            const modelCollection = this.firestore.collection(this.COLLECTION);

            await modelCollection.where("company_id", "==", company_id)
                .orderBy('is_default', 'desc')
                .orderBy('updatedAt')
                .get()
                .then(async (querySnapshot) => {
                    if (querySnapshot.empty == true) {
                        await that.createPipelineDefault(company_id).then((pipeline) => {
                            results.push(pipeline);
                        });
                    } else {
                        querySnapshot.forEach(docSnapshot => {
                            let record = new Pipeline();
                            Object.assign(record, docSnapshot.data());
                            record.id = docSnapshot.id;
                            results.push(record);
                        });
                    }
                })
        }
        return await results;
    }

    //override
    async insert(): Promise<Pipeline> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let result = {...this};
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.created_date = Date.now(); //TIMESTAMP;
        result.updated_date = Date.now(); //TIMESTAMP;

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.createdAt = Date.now(); //TIMESTAMP;
        result.updatedAt = Date.now(); //TIMESTAMP;

        result.updated_by = store.getters.currentUser.id;
        result.created_by = store.getters.currentUser.id;


        const model = await modelCollection.add(JSON.parse(JSON.stringify(result)));
        this.id = model.id;

        return this;

    }

    async update(): Promise<Pipeline> {

        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = Date.now();
        result.updated_by = store.getters.currentUser.id;

        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        return this;
    }

    constructor() {
        super("Pipeline");
    }

}
