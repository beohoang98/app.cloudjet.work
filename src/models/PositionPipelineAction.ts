import Model from './base';
import {store} from "@/store/store";

export default class PositionPipelineAction extends Model {

    position_id: string = '';
    pipeline_id: string = '';
    stage_id: string;
    actions: [];

    constructor() {
        super("PositionPipelineAction");
    }

    public async getAll(position_id): Promise<PositionPipelineAction[]> {
        const results: PositionPipelineAction[] = new Array();

        const modelCollection = this.firestore.collection(this.COLLECTION);
        const records = await modelCollection.where("position_id", "==", position_id)
            .orderBy("createdAt", "desc")
            .limit(20)
            .get();
        records.forEach(docSnapshot => {
            let record = new PositionPipelineAction();
            Object.assign(record, docSnapshot.data());
            record.id = docSnapshot.id;
            results.push(record);
        });

        return results
    }
    public async getByStageID(stage_id: string): Promise<PositionPipelineAction[]> {
        const results: PositionPipelineAction[] = new Array();

        const modelCollection = this.firestore.collection(this.COLLECTION);
        const records = await modelCollection.where("stage_id", "==", stage_id)
            .get();
        records.forEach(docSnapshot => {
            let record = new PositionPipelineAction();
            Object.assign(record, docSnapshot.data());
            record.id = docSnapshot.id;
            results.push(record);
        });

        return results;
    }
    public async getActionsImported(position_id: string, pipeline_id: string): Promise<PositionPipelineAction[]> {
        const results: PositionPipelineAction[] = new Array();

        const modelCollection = this.firestore.collection(this.COLLECTION);
        const records = await modelCollection.where("position_id", "==", position_id)
            .where("pipeline_id", "==", pipeline_id)
            .get();
        records.forEach(docSnapshot => {
            let record = new PositionPipelineAction();
            Object.assign(record, docSnapshot.data());
            record.id = docSnapshot.id;
            results.push(record);
        });

        return results;
    }

    public async getActionsByPositionStage(position_id: string, stage_id: string): Promise<PositionPipelineAction> {
        let result:PositionPipelineAction = new PositionPipelineAction();

        const modelCollection = this.firestore.collection(this.COLLECTION);
        const records = await modelCollection.where("position_id", "==", position_id)
            .where("stage_id", "==", stage_id)
            .get();

        if(!records.empty){
            let docSnapshot=records.docs[0];
            let record = new PositionPipelineAction();
            Object.assign(record, docSnapshot.data());
            record.id = docSnapshot.id;
            result=record
        }


        return result;
    }

    public async getItemByPipelineID(pipeline_id: string): Promise<PositionPipelineAction[]> {
        const results: PositionPipelineAction[] = new Array();

        const modelCollection = this.firestore.collection(this.COLLECTION);
        const records = await modelCollection.where("pipeline_id", "==", pipeline_id)
            .get();
        records.forEach(docSnapshot => {
            let record = new PositionPipelineAction();
            Object.assign(record, docSnapshot.data());
            record.id = docSnapshot.id;
            results.push(record);
        });

        return results;
    }


}
