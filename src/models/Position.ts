import Model from './base';
import {store} from "@/store/store";

export const PositionTypes = [
    {
        title: 'Fulltime',
        value: 'fulltime'
    },
    {
        title: 'Part time',
        value: 'parttime'
    },
    {
        title: 'Contract',
        value: 'contract',
    },
    {
        title: 'Temporary',
        value: 'temporary'
    },
    {
        title: 'Other',
        value: 'other'
    }
];


export const Education = [
    {title: "Unspecifiled", value: "unspecifiled"},
    {title: "Trung học cơ sở", value: "Trung học cơ sở"},
    {title: "Trung học phổ thông", value: "Trung học phổ thông"},
    {title: "Trung cấp", value: "Trung cấp"},
    {title: "Cao đẳng", value: "Cao đẳng"},
    {title: "Đại học ", value: "Đại học "},
    {title: "Thạc sĩ", value: "Thạc sĩ"},
    {title: "Tiến sĩ", value: "Tiến sĩ"},
    {title: "Chứng chỉ liên quan", value: "Chứng chỉ liên quan"},
];
export const DefaultApplicationForm = {
    coverletter: 1,
    email: 0,
    education: 1,
    experience: 1,
    name: 0,
    phone: 1,
    resume: 1,
    workhistory: 1
}
export const Experience = [
    {
        title: 'Not applicable',
        value: 'notapplicable'
    },
    {
        title: 'Internship',
        value: 'internship'
    },
    {
        title: 'Entry Level',
        value: 'entrylevel',
    },
    {
        title: 'Associate',
        value: 'associate'
    },
    {
        title: 'Mid Level',
        value: 'midlevel'
    },
    {
        title: 'Senior',
        value: 'senior'
    },
    {
        title: 'Executive',
        value: 'executive'
    },
];

export default class Position extends Model {

    title: string = "";
    company_id: string = "";
    department: string = "";
    internal_id: string = "";
    location: object = {
        title: '',
        value: ''
    };
    position_type: object = {};
    category: object = {};
    education: object = {};
    experience: object = {};
    job_description: object = {};
    state: string = "";
    tags: Array<object> = [];
    other_information: Array<object> = [];
    is_remote: boolean = false;
    candidate_type: string = '';
    application_form: {};
    questionnaire_id: string = '';
    position_pipeline_action: Array<object> = [];
    pipeline_id: string = '';
    automated_email_sender: string = "";
    scorecard_id: string = '';
    status: string = "draft";

    constructor() {
        super("Position");
    }

    static getFormatedLocationPositions(positions:Position[]):{}[] {

        let formatedPostionLocations = positions.map((position) => {
            let formated_location = []
            let formated_postion = position.cloneNewObject()

            formated_location = [position.location['title']]
            if (position.state) {
                formated_location.unshift(position.state)
            }
            formated_postion['formated_location'] = formated_location.join(', ').trim()

            return formated_postion
        })

        return formatedPostionLocations

    }
    public async getAllPositions(company_object: object = null): Promise<Position[]> {
        const results: Position[] = new Array();
        let company = store.getters.currentCompany;
        if (company_object != null) company = company_object;
        if (company) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("company_id", "==", company.id)
                .orderBy("createdAt", "desc")
                .limit(20)
                .get();
            records.forEach(docSnapshot => {
                let record = new Position();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);
            });
        }

        return results
    }

    public async getPublishPositions(company_id): Promise<Position[]> {
        const results: Position[] = new Array();
        if (company_id) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("company_id", "==", company_id)
                .where("status", "==", "published")
                .get();
            records.forEach(docSnapshot => {
                let record = new Position();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);
            });
        }
        return results
    }

}
