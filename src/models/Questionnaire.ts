import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import User from "@/models/User";

export default class Questionnaire extends Model {
    name: string = "";
    company_id: string = "";
    has_move_rule: Boolean = false;
    // templateVariables = null;
    // message_template: String = "";

    constructor() {
        super("questionnaire");
    }

    public async getCompany(): Promise<Company> {
        return (new Company()).getById(this.company_id) as Promise<Company>;
    }

    public async getAllQuestionnaires(): Promise<Questionnaire[]> {
        const results: Questionnaire[] = new Array();

        let company = store.getters.currentCompany;

        if (company) {

            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("company_id", "==", company.id)
                .limit(20)
                .get();

            records.forEach(docSnapshot => {

                let record = new Questionnaire();

                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);

            });
        }
        return results
    }
}
