import Model from './base';
import {messages} from "@/messages";
import {store} from "@/store/store";
import pipeline from "@/components/company/pipeline.vue";


const time_plan = [
    {
        value: 1,
        title: 'No delay'
    },
    {
        value: 2,
        title: 'Delay 1 hour'
    },
    {
        value: 3,
        title: 'Delay 4 hour'
    },
    {
        value: 4,
        title: 'Delay 2 days'
    },
    {
        value: 5,
        title: 'Delay 3 days'
    },
    {
        value: 6,
        title: 'Delay 1 week'
    },
    {
        value: 7,
        title: 'Delay 2 week'
    },
    {
        value: 8,
        title: 'Delay 1 month'
    },
    {
        value: 9,
        title: 'Delay 6 months'
    },
]
export const custom_data_action = {
    emails: {
        placeholder: 'Select a Template',
        template: [],
        title: 'Send Email',
        content: 'Automatically send an email to candidates entering this stage.',
        optional: [
            {
                value: null,
                title: 'Specify Sender Name (Optional)',
                description: 'Set a specific name to use as the sender of this email.',
                status: false,
                isInput: true,
            },
            {
                value: null,
                title: 'No-Reply Email Address (Optional)',
                description: 'Send from an email address that doesn\'t allow replies.',
                status: false,
                isInput: false
            }
            ],
        time_plan: time_plan
    },
    questionnaires: {
        title: 'Send Questionnaire',
        content: 'Automatically send a questionnaire to candidates entering this stage.',
        placeholder: 'Select a Questionnaire',
        template: [],
        optional: [],
        time_plan:time_plan
    },
    candicate_scorecard: {
        title: 'Candidate Scorecards',
        content: 'Automatically assign tasks to hiring team members to complete scorecards for candidates entering this stage.',
        // template: [],
        time_plan: [],
        optional: [],
    },
}

export default class PipelineStage extends Model {

    title: string = "";
    pipeline_id: string = "";
    stage_type: string = "";
    index: number = 0;
    icon: string = "";
    hide: boolean = false; // Có 1 stage default applied là không cho phép xóa.
    showInPositionPipelineStageModal: boolean = true; // Có 1 số item thuộc list default stage không hiển thị.
    actions: Array<object> = [];  // State actions

    // Cần phải có list stage default với pipeline_id là default.
    // => Cần define các field mô tả như icon, title.
    // 1. Khi tạo mới 1 pipeline (PL), việc khởi tạo sẽ yêu cầu có những stage mặc định.
    // 2. Chỉ sử dụng Vuex để lưu, vì giữa các modal còn xử lý việc lưu tạm object nữa
    //     nên code rất dài so với việc lưu default trên DB và xử lý việc tạm ở Vuex.

    public async getAllStages(pipeline_id: string): Promise<PipelineStage[]> {
        const results: PipelineStage[] = new Array();
        const modelCollection = this.firestore.collection(this.COLLECTION);
        await modelCollection
            .where("pipeline_id", "==", pipeline_id)
            .orderBy('index', 'asc')
            .limit(20).get().then(async (querySnapshot) => {
                if (querySnapshot.empty == true) {
                    const listState = [
                        {
                            title: messages.SR_007_MODAL_PIPELINE_STAGE_APPLIED,
                            icon: 'fa fa-user',
                            value: 'applied',
                            hide: true,
                            showInPositionPipelineStageModal: true,
                        },
                        {
                            title: messages.SR_007_MODAL_PIPELINE_STAGE_FEEDBACK,
                            icon: 'fa fa-users',
                            value: 'feedback',
                            showInPositionPipelineStageModal: true,
                        },
                        {
                            title: messages.SR_007_MODAL_PIPELINE_STAGE_INTERVIEW,
                            icon: 'fa fa-comments',
                            value: 'interview',
                            showInPositionPipelineStageModal: true,
                        },
                        {
                            title: messages.SR_007_MODAL_PIPELINE_STAGE_PHONESCREEN,
                            icon: 'fa fa-phone',
                            value: 'phonescreen',
                            showInPositionPipelineStageModal: false,
                        },
                        {
                            title: messages.SR_007_MODAL_PIPELINE_STAGE_MADEOFFER,
                            icon: 'fa fa-gift',
                            value: 'madeoffer',
                            showInPositionPipelineStageModal: true,
                        },
                        {
                            title: messages.SR_007_MODAL_PIPELINE_STAGE_DISQUALIFIED,
                            icon: 'fa fa-trash',
                            value: 'disqualified',
                            showInPositionPipelineStageModal: true,
                        },
                        {
                            title: messages.SR_007_MODAL_PIPELINE_STAGE_HIRED,
                            icon: 'fa fa-trophy',
                            value: 'hired',
                            showInPositionPipelineStageModal: true,
                        },
                        {
                            title: messages.SR_007_MODAL_PIPELINE_STAGE_SOURCED,
                            icon: 'fa fa-upload',
                            value: 'sourced',
                            showInPositionPipelineStageModal: false,
                        },
                        {
                            title: messages.SR_007_MODAL_PIPELINE_STAGE_OTHER,
                            icon: 'fa fa-list-ul',
                            value: 'other',
                            showInPositionPipelineStageModal: false,
                        }
                    ]
                    const arrayP = listState.map(async (state, index) => {
                        let _newStage = new PipelineStage();
                        if (state.showInPositionPipelineStageModal == true) _newStage.index = index + 1;
                        if (!state.showInPositionPipelineStageModal && pipeline_id != 'default'){
                            return state;
                        }
                        _newStage.title = state.title;
                        _newStage.stage_type = state.value;
                        _newStage.pipeline_id = pipeline_id;
                        _newStage.icon = state.icon;
                        _newStage.showInPositionPipelineStageModal = state.showInPositionPipelineStageModal;
                        _newStage.hide = !!state.hide; // stage.hide = 'abc'  --> !hide == false --> !!hide == true.
                        await _newStage.insert().then((new_stage) => {
                            let _stage = new PipelineStage();
                            Object.assign(_stage, new_stage);
                            results.push(_stage);
                        });
                        return state;
                    })
                    await Promise.all(arrayP)

                } else {
                    await querySnapshot.forEach(docSnapshot => {
                        let stage = new PipelineStage()
                        Object.assign(stage, docSnapshot.data());
                        stage.id = docSnapshot.id;
                        results.push(stage);
                    });
                }
            })
        return await results
    }

    constructor() {
        super("PipelineStage");
    }


}
