import Model from './base';
import MessagingTemplate from "@/models/MessagingTemplate";
import {messages} from "@/messages";


let templates = [
    {
        "name": messages.SR_052_EMAIL_TEMPLATE_2_NAME,
        "content": messages.SR_052_EMAIL_TEMPLATE_2_CONTENT,
    },
    {
        "name": messages.SR_052_EMAIL_TEMPLATE_3_NAME,
        "content": messages.SR_052_EMAIL_TEMPLATE_3_CONTENT,
    },
    {
        "name": messages.SR_052_EMAIL_TEMPLATE_4_NAME,
        "content": messages.SR_052_EMAIL_TEMPLATE_4_CONTENT,
    },
    {
        "name": messages.SR_052_EMAIL_TEMPLATE_5_NAME,
        "content": messages.SR_052_EMAIL_TEMPLATE_5_CONTENT,
    },
    {
        "name": messages.SR_052_EMAIL_TEMPLATE_6_NAME,
        "content": messages.SR_052_EMAIL_TEMPLATE_6_CONTENT,
    },

]
export default class EmailTemplate extends Model {

    messaging_template_id: string = "";
    name = "";
    subject = "";
    content = "";
    attachments = [];

    constructor() {
        super("email_template");
    }

    public async getEmailTemplates(messaging_template_id: string): Promise<EmailTemplate[]> {
        const results: EmailTemplate[] = [];
        const records = await this.collection()
            .where("messaging_template_id", "==", messaging_template_id)
            .get();

        if (records.empty) {
            return this.createDefaultEmailTemplate(messaging_template_id);
        }
        records.forEach(docSnapshot => {
            let record = new EmailTemplate();
            record.id = docSnapshot.id;
            Object.assign(record, docSnapshot.data());
            results.push(record);
        });
        return results
    }

    public async createDefaultEmailTemplate(messaging_template_id: string): Promise<EmailTemplate[]> {
        let results: EmailTemplate[] = [];
        let messaging_template = await new MessagingTemplate().getById(messaging_template_id) as MessagingTemplate;
        templates.forEach( (template) => {
            let _template = new EmailTemplate()
            Object.assign(_template, template);
            _template.subject = messaging_template.default_subject;
            _template.messaging_template_id = messaging_template_id;
            _template.content = _template.content.replace(/\n/g, "</br>");
            _template.insert().then((result) => {
                results.push(result as EmailTemplate);
            })
        })
        return results;
    }
}
