import Model from './base';
import {store} from "@/store/store";


export class criteria {
    criteria_name:string="";
    constructor(text: string) {
        this.criteria_name = text;
    }
}

export class section {
    section_name:string="";
    list_criteria:criteria[] =[];

    constructor(text: string,criteria: criteria[] ) {
        this.section_name = text;
        this.list_criteria = criteria;
    }

}

export default class Scorecard extends Model {
    scorecard_name:string="";
    company_id:string = "";
    sections:section[] = [
        {
            section_name: "",
            list_criteria: [
                {criteria_name: ""
                  ,
                }
            ]
        },
    ];

    constructor() {
        super("scorecards");
    }

    static get instance(): Scorecard {
        return new Scorecard();
    }

    //override
    async insert(): Promise<Scorecard> {
        const modelCollection = this.firestore.collection(this.COLLECTION);
        let result = {...this};
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.created_date = Date.now(); //TIMESTAMP;
        result.updated_date = Date.now(); //TIMESTAMP;

        //const {TIMESTAMP} = firebase.database.ServerValue;
        result.createdAt = Date.now(); //TIMESTAMP;
        result.updatedAt = Date.now(); //TIMESTAMP;

        result.updated_by =  store.getters.currentUser.id;
        result.created_by = store.getters.currentUser.id;


        const model = await modelCollection.add(JSON.parse(JSON.stringify(result)));
        this.id = model.id;

        return this;

    }

    async update(): Promise<Scorecard> {

        const modelCollection = this.firestore.collection(this.COLLECTION);

        let result = {...this};
        let id = result['id'];
        delete result['firestore'];
        delete result['COLLECTION'];
        delete result['id'];

        // const {TIMESTAMP} = firebase.database.ServerValue;
        result.updated_date = Date.now();
        result.updated_by =  store.getters.currentUser.id;

        // @ts-ignore
        const model = await modelCollection.doc(id).set(JSON.parse(JSON.stringify(result), {merge: true}));

        return this;
    }

    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }

    public async getAllScoreCard(): Promise<Scorecard[]> {
        const results: Scorecard[] = new Array();
        let company = store.getters.currentCompany;

        if (company) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("company_id", "==", company.id)
                .orderBy("createdAt", "desc")
                .limit(20)
                .get();
            records.forEach(docSnapshot => {
                let record = new Scorecard();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);
            });
        }

        return results
    }
}
