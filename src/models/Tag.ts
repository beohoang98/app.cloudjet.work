import Model from './base';
import {store} from "@/store/store";

export default class Tag extends Model {

    title: string = "";
    company_id: string = "";

    constructor() {
        super("Tag");
    }

    public async getAllTag(): Promise<Tag[]> {
        const results: Tag[] = new Array();
        let company = store.getters.currentCompany;

        if (company) {
            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("company_id", "==", company.id)
                .orderBy("createdAt", "desc")
                .limit(20)
                .get();
            records.forEach(docSnapshot => {
                let record = new Tag();
                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);
            });
        }

        return results
    }


}
