import Model from './base';
import Company from "@/models/Company";
import {store} from "@/store/store";
import User from "@/models/User";
import Questionnaire from "@/models/Questionnaire";

export default class Candidate extends Model {
    name: string = "";
    company_id: string = "";
    seen:boolean = false;
    position = {
        "_id" : "",
        "name" : "",
        "location" : {
            "name" : ""
        },
    };
    assignee_id: string = "";
    photo: string = '';
    address: string = "";
    cover_letter: string ="";
    education = [];
    email_address: string = "";
    headline: string = "";
    initial: string ="";
    origin: string = ""; //applied | recruiter | referral | sourced
    overall_score =  {
        "very_good": [], //User._id
        "good": [],//User._id
        "neutral": [],//User._id
        "poor": [],//User._id
        "very_poor": [],//User._id
        "scored_count":  0,
        "score": 0,
    };
    phone_number: string = "";
    profile_photo_url: string = "";
    questionnaire: Questionnaire[] = [];
    recruited_by = {
        "_id": "",
        "email_address": "",
        "name": ""
    };
    referred_by = {
        "_id":  "",
        "email_address":  "",
        "name": ""
    };
    sourced_by = {
        "_id":  "",
        "email_address":  "",
        "name":  ""
    };
    resume =  {
        "file_name": "",
        "url":  "",
        "pdf_url":  ""
    };
    social_profiles = [
        {
            "type":  "",
            "typeId": "",
            "typeName": "",
            "url":  ""
        }
    ];
    source =  {
        "id": "",
        "name": "",
        "type": ""
    };
    stage = {
        "id": "",
        "name": "",
        "type": ""
    };

    stage_actions_enabled:boolean = false

    summary: string ="";
    tags = [];
    work_history = [
        {
            "company_name":  "",
            "title": "",
            "summary":  "",
            "is_current": true,
            "start_date": {
                "month":  "",
                "year":  ""
            },
            "end_date": {
                "month":  "",
                "year":  ""
            }
        }
    ];
    custom_attributes = [
        {
            "name": "",
            "id": "",
            "secure":  true,
            "value": ""
        }
    ];
    disposition_date: Date;
    disposition_reason = {
        "_id":"",
        "name": "",
    };


    constructor() {
        super("candidates");
    }

    static get instance(): Candidate {
        return new Candidate();
    }

    public async getAssignee(): Promise<User> {
        return (new User()).getById(this.assignee_id) as Promise<User>;
    }


    public async getCompany(): Promise<Company> {

        return (new Company()).getById(this.company_id) as Promise<Company>;
    }

    public async getAllCandidates(): Promise<Candidate[]> {
        const results: Candidate[] = new Array();

        let company = store.getters.currentCompany;

        if (company) {

            const modelCollection = this.firestore.collection(this.COLLECTION);
            const records = await modelCollection.where("company_id", "==", company.id)
            // TODO  .orderBy("updatedAt", "desc")
                .limit(20)
                .get();


            records.forEach(docSnapshot => {

                let record = new Candidate();

                Object.assign(record, docSnapshot.data());
                record.id = docSnapshot.id;
                results.push(record);

            });
        }
        // console.info(`Candidates:`);
        // console.info(results);


        return results
    }

    static listenRealtime(company_id?: string): void {
        if (company_id) {
            this.instance.collection().where("company_id", '==', company_id)
                .onSnapshot((snapshots) => {
                    snapshots.docChanges().forEach((change) => {
                        const model = this.from(change.doc.data(), change.doc.id);
                        switch (change.type) {
                            case "added":
                                this._onAdded(model);
                                break;
                            case "modified":
                                this._onUpdated(model);
                                break;
                            case "removed":
                                this._onDeleted(model);
                            default:
                                break;
                        }
                    });
                });
        }
    }

}
