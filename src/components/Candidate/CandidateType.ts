import {messages} from "@/messages";

export default [

    {
        "text": messages.SR_028_CANDIDATE_FILTER_ALL,
        "value": "all"
    },
    {
        "text": messages.SR_028_CANDIDATE_FILTER_NEW,
        "value": "new"
    },
    {
        "text": messages.SR_028_CANDIDATE_FILTER_MY,
        "value": "mine"
    },
    {
        "text": messages.SR_028_CANDIDATE_FILTER_STAR,
        "value": "starred"
    },
    {
        "text": messages.SR_028_CANDIDATE_FILTER_UNSEEN,
        "value": "unseen"
    }
]
