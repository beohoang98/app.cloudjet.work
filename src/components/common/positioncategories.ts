export const positioncategories = [
    {
        "value": "software",
        "name": "Software Development"
    },
    {
        "value": "design",
        "name": "Interactive Design"
    },
    {
        "value": "product",
        "name": "Product Management"
    },
    {
        "value": "sysadmin",
        "name": "System Administration"
    },
    {
        "value": "devops",
        "name": "DevOps"
    },
    {
        "value": "finance",
        "name": "Finance"
    },
    {
        "value": "custserv",
        "name": "Customer Service"
    },
    {
        "value": "sales",
        "name": "Sales"
    },
    {
        "value": "marketing",
        "name": "Marketing"
    },
    {
        "value": "pr",
        "name": "PR / Communications"
    },
    {
        "value": "design-not-interactive",
        "name": "Design"
    },
    {
        "value": "hr",
        "name": "Human Resources"
    },
    {
        "value": "management",
        "name": "Management"
    },
    {
        "value": "operations",
        "name": "Operations"
    },
    {
        "value": "other",
        "name": "Other"
    }
];
