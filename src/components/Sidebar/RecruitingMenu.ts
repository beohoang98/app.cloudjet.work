import {messages} from "@/messages";

export default [
    [
        {
            icon: "globe",
            text: "Cài đặt trang tuyển dụng",
            to: 'portal-type',
        },
        // {
        //     icon: "briefcase",
        //     text: "Cấu hình  Portal Setting",
        //     to: 'portal-employees',
        // },
        {
            icon: "comments",
            text: "Cấu hình thông điệp",
            to: 'portal-messaging',
        },
    ],
    [
        {
            icon: "columns",
            text: "Pipeline Stages",
            to: 'setting-pipeline',
        },
        {
            icon: "book",
            text: "Interview Guides",
            to: 'setting-interview-guides',
        },
        {
            icon: "list",
            text: "Questionnaires",
            to: 'setting-questionnaires',
        },
        {
            icon: "thumbs-up",
            text: messages.SR_053_TITLE,
            to: 'setting-scorecards',
        },
        {
            icon: "envelope",
            text: "Message Template",
            to: 'messages-template',
        },
    ]
    /// ....
]
