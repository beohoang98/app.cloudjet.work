import Home from "@/views/Home.vue";
import {RouteConfig} from 'vue-router';

const Route: RouteConfig = {
    path: '/candidate/:candidate_id',
    redirect: '/candidate/:candidate_id/experience',
    name: 'candidate_detail',
    components: {
        default: Home,
        modal_content: () => import('@/components/modals/Candidate/CandidateModal.vue'),
    },
    meta: {
        showModal: true,
        modalMaxWidth: 1200,
    },
    children: [
        {
            path: 'details',
            name: 'CandidateTabDetail',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Detail.vue'),
            },
        },
        {
            path: 'resume',
            name: 'CandidateTabResume',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Resume.vue'),
            },
        },
        {
            path: 'experience',
            name: 'CandidateTabExperience',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Experience.vue'),
            },
        },
        {
            path: 'attachments',
            name: 'CandidateTabAttachments',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Attachments.vue'),
            },
        },
        {
            path: 'discussion',
            name: 'CandidateTabDiscussion',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Discussion.vue'),
            },
        },
        {
            path: 'notes',
            name: 'CandidateTabNotes',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Notes.vue'),
            },
        },
        {
            path: 'scorecard',
            name: 'CandidateTabScorecard',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Scorecards.vue'),
            },
        },
        {
            path: 'email',
            name: 'CandidateTabEmail',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Email.vue'),
            },
        },
        {
            path: 'tasks',
            name: 'CandidateTabTasks',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Tasks.vue'),
            },
        },
        {
            path: 'questionnaires',
            name: 'CandidateTabQuestionnaires',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Questionnaires.vue'),
            },
        },
        {
            path: 'calendar',
            name: 'CandidateTabCalendar',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Calendar.vue'),
            },
        },
        {
            path: 'references',
            name: 'CandidateTabReferences',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/References.vue'),
            },
        },
        {
            path: 'assessments',
            name: 'CandidateTabAssessments',
            components: {
                modal_child: ()=>import('@/components/modals/Candidate/Tab/Assessment.vue'),
            },
        },
    ],
};

export default Route;
