import LandingPage from "@/views/LandingPage.vue"
import DetailPosition from "@/components/LandingPage/DetailPosition/DetailPosition.vue"
import LandingPageHome from "@/components/LandingPage/homeLandingPage.vue"
import {RouteConfig} from 'vue-router';

import LandingPageHomeSimple from "@/components/LandingPage/SimplePage/simple.vue"

const Route: RouteConfig = {
    path: '/w/:slug',
    name: 'LandingPage',
    components: {
        default: LandingPage,
    },
    meta: {
        noAuth: true,
        noSidebar: true,
    },
    children: [
        {
            path: '/',
            name: 'LandingPageHome',
            components: {
                default: LandingPageHome,
            },
            meta: {
                noAuth: true,
                noSidebar: true,
            },
        },{
            path: 'position/:position_id/',
            name: 'LandingPageDetailPosition',
            components: {
                default: DetailPosition,
            },
             meta: {
                noAuth: true,
                noSidebar: true,
            },
        },

    ],
};

export default Route;
