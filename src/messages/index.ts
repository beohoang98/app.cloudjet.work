import {common} from "@/messages/common";
import {sr_001} from "@/messages/SR_001";
import {sr_002} from "@/messages/SR_002";
import {sr_003} from "@/messages/SR_003";
import {sr_005} from "@/messages/SR_005";
import {sr_006} from "@/messages/SR_006"
import {SR_007} from "@/messages/SR_007";
import {SR_009} from "@/messages/SR_009";
import {SR_032} from "@/messages/SR_032";
import {SR_052} from "@/messages/SR_052";
import {sr_053} from "@/messages/SR_053";
import {sr_028} from "@/messages/SR_028";
import {SR_020} from "@/messages/SR_020";
import {SR_046} from "@/messages/SR_046";
import {SR_047} from "@/messages/SR_047";

export const messages = {
    ...common,
    ...sr_001,
    ...sr_002,
    ...sr_003,
    ...sr_005,
    ...sr_006,
    ...SR_007,
    ...SR_009,
    ...SR_032,
    ...SR_052,
    ...sr_053,
    ...sr_028,
    ...SR_020,
    ...SR_046,
    ...SR_047,

}
