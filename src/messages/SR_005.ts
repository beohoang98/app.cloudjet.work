export const sr_005 = {
    SR_005_QUESTIONNAIRES:"Questionnaires",
    SR_005_CONTENT_QUESTIONNAIRES:"Questionnaires are collections or one or more custom questions you'd like to ask candidates during the application process or in later stages with Stage Actions.",
    SR_005_LEARN_MORE:	"Learn More",
    SR_005_CREATED_ANY_QUESTIONNAIRES:	"You haven't created any questionnaires.",
    SR_005_SORT_DEFAULT:	"Default",
    SR_005_SORT_ALPHABETICAL:	"Alphabetical",
    SR_005_ADD_QUESTIONNAIRE:	"Add Questionnaire",
    SR_005_INPUT_QUESTIONNAIRE_NAME:	"Questionnaire Name (Required)",
    SR_005_lEAST_1_QUESTION:	"You must have at least 1 question.",
    SR_005_NO_QUESTIONS:	"No Questions",
    SR_005_REQUIRED:	"Required",
    SR_005_ADD_QUESTION:	"Add Question",
    SR_005_MOVE_ON_COMPLETION:	"Move on Completion",
    SR_005_MOVE_ON_COMPLETION_CONTENT_1:	"Move the candidate to another stage after completing this questionnaire.",
    SR_005_MOVE_ON_COMPLETION_CONTENT_2:	"* Ignored when questionnaire is used in application form.",
    SR_005_MOVE_TO_STAGE:"Move to Stage",
    SR_005_QUESTIONNAIRE_EMAIL_TEMPLATE:	"Questionnaire Email Template",
    SR_005_QUESTIONNAIRE_EMAIL_TEMPLATE_CONTENT:	"This template is used when sending this assessment to an existing candidate.",
    SR_005_MESSAGE_TEMPLATE:	"Enter a message template",
    SR_005_BTN_SAVE_CHANGE:	"Save Change",
    SR_005_BTN_ADD_QUESTIONNAIRE:	"Add Questionnaire",
    SR_005_TEMPLATE_VARIABLES:	"Template Variables",
    SR_005_CANDIDATE_FIRST_NAME:	"Candidate First Name",
    SR_005_CANDIDATE_FIRST_NAME_VALUE:	"candidate_firstname",
    SR_005_CANDIDATE_FULL_NAME:	"Candidate Full Name",
    SR_005_CANDIDATE_FULL_NAME_VALUE:	"candidate_fullname",
    SR_005_CANDIDATE_EMAIL_ADDRESS:	"Candidate Email Address",
    SR_005_CANDIDATE_EMAIL_ADDRESS_VALUE:	"candidate_email_address",
    SR_005_POSITION_TITLE:	"Position Title",
    SR_005_POSITION_TITLE_VALUE:	"position_title",
    SR_005_POSITION_LINK:	"Position Link",
    SR_005_POSITION_LINK_VALUE:	"position_link",
    SR_005_COMPANY_NAME:	"Company Name",
    SR_005_COMPANY_NAME_VALUE:	"company_name",
    SR_005_COMPANY_USER:	"Company User",
    SR_005_COMPANY_USER_VALUE:	"company_user",
    SR_005_COMPANY_USER_FIRST_NAME:	"Company User First Name",
    SR_005_COMPANY_USER_FIRST_NAME_VALUE:	"company_user_first_name",
    SR_005_QUESTIONNAIRE_LINK:	"Questionnaire Link",
    SR_005_QUESTIONNAIRE_LINK_VALUE:	"questionnaire_link",
    SR_005_SHOW_TEMPLATE:	"Show Template",
    SR_005_HIDE_TEMPLATE:	"Hide Template",
    SR_005_INPUT_QUESTION:	"Enter your question",
    SR_005_TEXTAREA_QUESTION_DESCRIPTION:	"Question description (optional)",
    SR_005_CHECKBOX_REQUIRED:	"Required",
    SR_005_MOVE_BASED_ON_ANSWER:	"Move based on answer",
    SR_005_FILEATTACHMENT:	"The candidate provided file attachment will be limited to 20MB.",
    SR_005_PIPELINES_STAGE:	"Pipelines / Stage",
    SR_005_ADD_OPTION:	"Add Option",
    SR_005_RESPONSE_TYPE:	"Response Type",
    SR_005_TEXT:	"Text",
    SR_005_PARAGRAPH:	"Paragraph",
    SR_005_MULTIPLE_CHOICE:	"Multiple Choice",
    SR_005_DROPDOWN_LIST:	"Dropdown List",
    SR_005_CHECKBOXES:	"Checkboxes",
    SR_005_FILE_ATTACHMENT:	"File Attachment",
    SR_005_EMAIL_TEMPLATE: "Hi [[candidate_first_name]],\n" +
        "\n" +
        "We'd like to ask you a few more questions. When you have a couple minutes, please click the following link and complete the form.\n" +
        "\n" +
        "[[questionnaire_link]]\n" +
        "\n" +
        "Thank you,\n" +
        "\n" +
        "[[company_user_first_name]]",
    SR_005_UNSAVED_CHANGES:	"Unsaved Changes",
    SR_005_UNSAVED_CHANGES_CONTENT:	"Would you like to save your changes to this questionnaire?",
    SR_005_DELETE_QUESTIONNAIRE:	"Delete Questionnaire?",
    SR_005_SR_005_DELETE_QUESTIONNAIRE_CONTENT:	"Are you sure you want to delete this Questionnaire?",
    SR_005_NO_THANKS:	"No thanks",
    SR_005_YES_PLEASE:	"Yes please",
    SR_005_EDIT_QUESTIONNAIRE: "Edit Questionnaire",
    SR_005_EDIT_QUESTION: "Edit Question",
}
