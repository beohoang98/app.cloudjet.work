export const SR_047 = {
    SR_047_MENU_JOB_OPENING: "Job Openings",
    SR_047_BTN_APPLY_POSITIONS:"Apply To Position",
    SR_047_BTN_INDEED:"Use My Indeed Resume",
    SR_047_BTN_LINKEDIN:"Apply Using LinkedIn",
    SR_047_BTN_INDEED_SIMPLE:"Use My Indeed Resume",
    SR_047_BTN_LINKEDIN_SIMPLE:"Apply using LinkedIn",
}
