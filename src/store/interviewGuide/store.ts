import {Action,MutationTree,Module,StoreOptions} from "vuex";
import Vuex from 'vuex';
import InterviewGuide from '@/models/InterviewGuides'

export interface InterviewGuides {
    listInterviewGuide: Array<object>,
    currentListInterviewGuide: Array<object>,
}

const store: Module<InterviewGuides ,any>={
    state: {
        currentListInterviewGuide:[],
        listInterviewGuide:[],
    },
    getters:{
        currentListInterviewGuide: (state)=>{
            return state.currentListInterviewGuide
        },
        listInterviewGuide:(state)=>{
            return state.listInterviewGuide
        },
    },
    mutations:{
        setListInterviewGuide(state,data){
            state.listInterviewGuide = data
        },
        pushListInterviewGuide(state,data){
            state.listInterviewGuide.unshift(data)
        },
        setCurrentListInterviewGuide(state,data){
            state.currentListInterviewGuide = data
            state.listInterviewGuide = data
        },
        removeInterviewGuide(state,index){
            state.listInterviewGuide.splice(index,1)
        }
    },
    actions:{
        editListInterviewGuide: async(context,data)=> {
            switch (data.type) {
                case 'remove':
                    console.log("statte",context.state.listInterviewGuide)
                        let index = context.state.listInterviewGuide
                        .findIndex(interview => interview['id'] == data.content.id
                    )
                    context.commit('removeInterviewGuide',index)
                    break
                case 'duplicate':
                    let _duplicate_interview_guide = new InterviewGuide()
                    Object.assign(_duplicate_interview_guide, data.content)
                    _duplicate_interview_guide.name+= " (Copy)"
                    _duplicate_interview_guide.insert().then((data)=>{
                        context.commit('pushListInterviewGuide',data)
                    })
                    break
                default:
                    //nothing
            }
        },
        setCurrentListInterviewGuide:async (context,data)=>{
            context.commit('setCurrentListInterviewGuide',data)
        },
        addInterviewGuide: async (context,data)=>{
            let _new_interview_guide = new InterviewGuide()
            Object.assign(_new_interview_guide,data)
            _new_interview_guide.insert().then((data)=>{
                context.commit('pushListInterviewGuide',data)
            })

        }
    }
}
export default store
