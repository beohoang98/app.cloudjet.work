import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vuex from 'vuex';
import Router from "@/router";
import Task from "@/models/Task"
import _ from "lodash";
import User from "@/models/User";
import {reject} from "q";
import {debug} from "webpack";


export interface Tasks {
    _tasks: Array<object>
}

const store: Module<Tasks, any> = {
    //namespaced: true,
    state: {
         _tasks: [],
    },
    mutations: {
        setTasks(state, data) {
            state._tasks = data;
        },
    },
    getters: {
        tasks(state){
            return state._tasks
        }

    },
    actions: {
        createdTasks: async (context, {task, users}) => {
            let list_users = users.map((user) => {
                return new Promise((resolve, reject) => {
                    let new_task = new Task();
                    Object.assign(new_task, task);
                    new_task.company_id = context.getters.currentCompany.id;
                    new_task.assignee = user.cloneNewObject();
                    new_task.assignee_id = user.id;
                    new_task.insert().then(() => {
                        resolve();
                    });
                })
            });
            return Promise.all(list_users);
        },

        updateTaskAction: async (context, {task, users}) =>{
            let update_task = new Task();
            if(users.length == 0){
                 Object.assign(update_task, task);
                 return update_task.update();
            }else {
                let res = users.map((user) => {
                    return new Promise((resolve, reject) => {
                        Object.assign(update_task, task);
                        if(update_task.assignee_id != user.id){
                            update_task.assignee = user.cloneNewObject();
                            update_task.assignee_id = user.id;
                        }
                        update_task.update().then(() => {
                            resolve();
                        })
                    })
                });
                return Promise.all(res);
            }
        },

        removeTask: async (context, task)=>{
            let _task = new Task();
            Object.assign(_task, task);
            return _task.delete();
        },

        getTask: async (context) => {
             new Task().getAllTasks().then((data) => {
                context.commit("setTasks", data)
            });
        },

        getFilteredTasks: async (context, {filter_field, value}) => {
            let tasks = await new Task().getFilteredTasks(filter_field, value);
            context.commit('setTasks', tasks);
        },

        getListTask: async (context) => {
            Task.listenRealtime();
            Task.onAdded((task) => {
                if (task.company_id != context.getters.currentCompany.id) {
                    return;
                }
                let index = context.state._tasks.findIndex((t: any) => t.id == task.id);
                if (index == -1) {
                    context.state._tasks.push(task);
                }
            });

            Task.onUpdated((task) => {
                if (task.company_id != context.getters.currentCompany.id) {
                    return;
                }
                let index = context.state._tasks.findIndex((t: any) => t.id == task.id);
                if (index != -1) {
                    let tasks = [...context.state._tasks];
                    tasks[index] = task;
                    context.state._tasks = tasks;
                }
            });

            Task.onDeleted((task) => {
                if (task.company_id != context.getters.currentCompany.id) {
                    return;
                }
                let index = context.state._tasks.findIndex((t: any) => t.id == task.id);
                if (index != -1) {
                    context.state._tasks.splice(index, 1);
                }
            });

        }
    }
};

export default store;
