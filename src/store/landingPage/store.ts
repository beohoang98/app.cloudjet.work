import {Module} from "vuex";
import Portal from "@/models/Portal";
import Company from "@/models/Company"
import Position from '@/models/Position'
import _ from 'lodash';
import {messages} from "@/messages";

export interface LandingPageState {
    portal_lading_page: object,
    company_landing_page:object,
    positions_landing_page: Array<object>,
    // list_all_postions:Array<object>,
    list_department:Array<object>,
    list_location:Array<object>,
    position_view:object,
    current_page:string
}

const store : Module<LandingPageState ,any> = { // any for test
    state: {
        portal_lading_page: {},
        company_landing_page:{},
        positions_landing_page:[],
        // list_all_postions: [],
        list_department:[],
        list_location:[],
        position_view:{},
        current_page:"Lading-home-page"
    },
    getters: {
        currentPortalLandingPage: state => {
            return state.portal_lading_page
        },
        currentCompanyLandingPage:state => {
            return state.company_landing_page
        },
        currentPositionLandingPage:state => {
            return state.positions_landing_page
        },
        currentDepartmentsLandingPage:state => {
            return state.list_department
        },
        currentLocationsLandingPage:state => {
            return state.list_location
        },
        convertLinkToEmbedLink:state=>{
            let link = ""
            if(state.portal_lading_page['vimeo_video']){

                link = state.portal_lading_page['vimeo_video'].replace("vimeo.com/","player.vimeo.com/video/")
            } else if(state.portal_lading_page['youtube_video']){
                link =  state.portal_lading_page['youtube_video'].replace("watch?v=","embed/")
            }else{
                link = 'https://player.vimeo.com/video/watch'
            }
            return link
        },
        currentPositionView:state => {
            return state.position_view
        },
        currentPage:state => {
            return state.current_page
        }
    },
    mutations: {
        setCurrentPortalLandingPage(state, portal) {
            state.portal_lading_page = portal;
        },
        setCurrentCompanyLandingPage(state, company) {
            state.company_landing_page = company;
        },
        setCurrentPositionsLandingPage(state, positions){
            state.positions_landing_page = positions
        },
        // setCurrentAllPositionsLandingPage(state,positions){
        //     state.list_all_postions = positions
        // },
        setCurrentDepartmentsLandingPage(state,departments){
            state.list_department = departments
        },
        setCurrentLocationsLandingPage(state,locations){
            state.list_location = locations
        },
        setCurrentPositionView(state, position){
            state.position_view = position
        },
        setCurrentNamePage(state,page_name){
            state.current_page = page_name
        }
    },
    actions: {
        getCompanyLandingPagebySlug: async(context,company_slug)=>{
            await new Company().getBySlug(company_slug).then((data)=>{
                    context.commit('setCurrentCompanyLandingPage',data)

                    // context.dispatch('getAllPositionsLandingPage',data)
                })
        },
        getPortalLandingPage: async (context,company_id) => {
            // await Portal.listenRealtime();
            await new Portal().getAllPortal(company_id).then((data)=>{
                    context.commit('setCurrentPortalLandingPage',data)
                })
        },
        getPositionsPublishLandingPage: async (context,company_id)=>{
            await new Position().getPublishPositions(company_id).then((data)=>{
                context.dispatch('AddFiledGroupLocations',data)


            })
        },
        // getAllPositionsLandingPage: async (context,company)=> {
        //     await new Position().getAllPositions(company).then((data)=>{
        //         context.commit('setCurrentAllPositionsLandingPage',data)
        //     })
        // },
        getDepartmentsLandingPage:async (context)=>{
            let new_group_by_departments = _.groupBy(context.state.positions_landing_page, 'department')
            let new_departments = Object.keys(new_group_by_departments).map((department)=>{
                let text_department = department
                if (department == '')  text_department = messages.SR_046_FILTER_DEPARTMENT_ORTHER
                return {
                    value: department,
                    text: text_department
                }
            })
            console.log(new_departments)
            context.commit('setCurrentDepartmentsLandingPage',new_departments)
        },
        getLocationsLandingPage:async (context)=>{
            let list_position = _.groupBy(context.state.positions_landing_page, 'group_location')
            let new_locations = Object.keys(list_position)
            console.log(new_locations)
            context.commit('setCurrentLocationsLandingPage',new_locations)
        },
        AddFiledGroupLocations:(context,list_positions)=>{
            let new_list_position = []
            if(list_positions.length > 0){
                new_list_position = list_positions.map(position => {

                    position['group_location'] = [position.location.title ]
                    if(position.state){
                        position['group_location'].unshift(position.state)
                    }
                    position['group_location'] = position['group_location'].join(', ').trim()

                    return position
                })
                context.commit('setCurrentPositionsLandingPage',new_list_position)
                context.dispatch('getDepartmentsLandingPage');
                context.dispatch('getLocationsLandingPage');
            }else{
                context.commit('setCurrentPositionsLandingPage',new_list_position)
            }
        },
        setPositionView: async (context, position_id)=>{
            await new Position().getById(position_id).then((data)=>{
                context.commit('setCurrentPositionView', data)
            })

        },
        checkCurrentNamePage:(context,name_page)=>{
            let current_name_page = "Lading-home-page"
            switch (name_page) {
                   case "LandingPageHome":
                       current_name_page = "Lading-home-page"
                       break
                   case "LandingPageDetailPosition":
                       current_name_page = "Landing-position-page"
                       break
                   default:
                       //nothing
               }
            context.commit('setCurrentNamePage', current_name_page)
        }

    },
};
export default store;
