import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vue from 'vue';
import Vuex from 'vuex';
import Position from "@/models/Position";
import {messages} from "@/messages";

Vue.use(Vuex);

export interface PositionStage {
    currentPosition: Object;
    steps: Array<object>;
    templatePosition: Object;
    listPosition: Array<object>;
    cachePosition: Object;
}

const store: Module<PositionStage, any> = {
    // namespaced: true,
    state: {
        templatePosition: {
            title: '',
            department: '',
            internal_id: '',
            job_description: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_12,
                'value': null,
                'description': '',
            },
            location: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_4,
                'value': null,
            },
            position_type: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_6,
                'value': null,
            },
            category: {
                'name': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_7,
                'value': null,
            },
            education: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_8,
            },
            experience: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_9,
                'value': null,
            },
            state: '',
            tags: [],
            other_information: [],
            is_remote: false,
            candidate_type: 'enabled',
            application_form: {},
            questionnaire_id: '',
            position_pipeline_action: [] ,
            pipeline_id: {},
            automated_email_sender: "",
            scorecard_id: '',
        },
        steps: [
            {
                title: messages.SR_020_BUTTON_STEP_TOOLTIP_1,
                active: true,
                done: false
            },
            {
                title: messages.SR_020_BUTTON_STEP_TOOLTIP_2,
                active: false,
                done: false
            },
            {
                title: messages.SR_020_BUTTON_STEP_TOOLTIP_3,
                active: false,
                done: false
            },

            {
                title: messages.SR_020_BUTTON_STEP_TOOLTIP_4,
                active: false,
                done: false
            },
        ],
        currentPosition: {
            job_description: {
                'title': messages.SR_020_MODAL_NEW_POSITION_PLACEHOLDER_12,
                'value': null,
                'description': '',
            },
        },
        listPosition: [],
        cachePosition: {}
    },
    mutations: {
        setCurrentPosition(state, data) {
            state.currentPosition = data;
        },
        setsteps(state, data) {
            state.steps = data;
        },
        setListPosition(state, data) {
            state.listPosition = data;
        },
        setCachePosition(state, data){
            state.cachePosition = data;
        }
    },
    getters: {
        currentPosition(state) {
            return state.currentPosition;
        },
        steps(state) {
            return state.steps;
        },
        templatePosition(state) {
            return state.templatePosition;
        },
        listPosition(state) {
            return state.listPosition;
        },
        cachePosition(state){
            return state.cachePosition;
        }
    },
    actions: {
        setStepByClick: (context, index)=>{
            let steps = context.getters['steps'];
            steps.map((step, _index)=>{
                if (index == _index){
                    step.active = true;
                }else{
                    step.active = false;
                }
                return step;
            })
            context.dispatch('setsteps', steps);
        },
        revertSteps: (context, data)=>{
            let steps = context.getters['steps'].map((step, index)=>{
                if (index == 0){
                    step.active = true;
                    step.done = false;
                }else{
                    step.active = false;
                }
                return step;
            })
            context.commit('setsteps', steps)
        },
        setCurrentPosition: async (context, data) => {
            context.commit("setCurrentPosition", data);
        },
        setsteps: async (context, data) => {
            context.commit("setsteps", data);
        },
        setListPosition: async (context, data)=>{
            context.commit("setListPosition", data);
        },
        getListPosition: async (context, data) => {
            new Position().getAllPositions().then((data)=>{
                context.dispatch('setListPosition', data);
            })
        },
    },
};

export default store;
