import {Action, MutationTree, StoreOptions, Module} from "vuex";
import Vue from 'vue';
import Vuex from 'vuex';
import Candidate from "@/models/Candidate";
// import PositionPipeline from "@/models/Pipeline";

Vue.use(Vuex);

export interface ICandidate {
    // listPipeline: Array<object>,
    // listPipelineStage: Array<object>,
    // currentPipeline: object,
    // currentPipelineStage: object,
    // cachePipeline: object
}

const store: Module<ICandidate, any> = { // wtf is this??
    // namespaced: true,
    state: {
        // listPipeline: [],
        // listPipelineStage: [],
        // currentPipeline: {},
        // currentPipelineStage: {},
        // cachePipeline: {},
    },
    mutations: {

    },
    getters: {

    },
    actions: {

    },
};

export default store;
