import {Module} from 'vuex';
import moment from "moment";

export interface IScheduleInterviewsState {
    open: boolean;

    candidate_id: string | null;

    startTime: number | null; // timestamp
    duration: number | null; // in minutes

    assignee_id: string | null;
    location: string | null;

    title: string | null;
    description: string | null;
}

const store: Module<IScheduleInterviewsState, any> = {
    namespaced: true,
    state: {
        open: false,

        candidate_id: null,

        startTime: Date.now(), // timestamp
        duration: 15, // in minutes

        assignee_id: null,
        location: null,

        title: null,
        description: null,
    },
    mutations: {
        setOpen: (state, val = true) => {
            state.open = val;
        },
        setCandidateId: (state, id: string) => {
            state.candidate_id = id;
        },

        setStartTime: (state, time: any) => {
            if (typeof time === 'string') {
                state.startTime = (new Date(time)).getTime();
            } else {
                state.startTime = moment(time).toDate().getTime();
            }
        },
        setDuration: (state, durationInMinutes: number) => {
            state.duration = durationInMinutes;
        },

        setAssigneeId: (state, id: string) => {
            state.assignee_id = id;
        },

        setLocation: (state, location: string) => {
            state.location = location;
        },

        setTitle: (state, title: string) => {
            state.title = title;
        },

        setDescription: (state, description: string) => {
            state.description = description;
        },
    },
    getters: {
        open: state => {
            return state.open;
        },

        candidate_id: state => {
            return state.candidate_id;
        },

        startTime: state => {
           return moment(state.startTime).toDate();
        },
        duration: state => {
            return moment.duration({
                minute: state.duration,
            }).humanize();
        }, // in minutes

        assignee_id: state => state.candidate_id,
        location: state => state.location,

        title: state => state.title,
        description: state => state.description,
    },

    actions: {

    },
};

export default store;
