import {Action, MutationTree, StoreOptions, Module} from "vuex";
import MessagingTemplate from "@/models/MessagingTemplate";
import EmailTemplate from "@/models/EmailTemplate";
import _ from "lodash";


export interface ModalState {
    _messaging_template: MessagingTemplate,
    _email_templates: Array<EmailTemplate>
}

const store: Module<ModalState, any> = {
    state: {
        _messaging_template: null,
        _email_templates: [],
    },
    mutations: {
        setMessagingTemplate(state, template) {
            state._messaging_template = template;
        },
        setEmailTemplates(state, email_templates) {
            state._email_templates = email_templates;
        }
    },
    getters: {
        messaging_template(state) {
            return state._messaging_template;
        },
        email_templates(state) {
            return state._email_templates;
        }
    },
    actions: {
        getMessagingTemplate: async (context, company_id: string) => {
            let template = await new MessagingTemplate().getMessagingTemplate(company_id);
            context.commit('setMessagingTemplate', template);
        },
        getEmailTemplates: async (context, messaging_template_id: string) => {
            let templates = await new EmailTemplate().getEmailTemplates(messaging_template_id);
            context.commit('setEmailTemplates', templates);
        },
        updateEmailTemplate: async (context, template: Object) => {
            let instance = new EmailTemplate()
            Object.assign(instance, template);
            instance.update().then( (obj: any) => {
                context.dispatch('getEmailTemplates', obj.messaging_template_id)
            })
        },
        updateMessagingTemplate: async (context, template: Object): Promise<any> => {
            let instance = new MessagingTemplate();
            Object.assign(instance, template);
            return instance.update()
        },
        createEmailTemplate: async (context, template: Object) => {
            let emailTemplate = new EmailTemplate()
            Object.assign(emailTemplate, template);
            emailTemplate.messaging_template_id = context.state._messaging_template.id;
            emailTemplate.insert().then( (obj) => {
                context.dispatch('getEmailTemplates', context.state._messaging_template.id);
            })
        },
        deleteEmailTemplate: async (context, email_template) => {
            let index = _.findIndex(context.state._email_templates, (e: any) => {
                return e.id == email_template.id;
            });
            if (index != -1) {
                email_template.delete();
                context.state._email_templates.splice(index, 1);
            }
        },
    }
};

export default store;
