import Vue from 'vue';
import Vuex from 'vuex';
import firebase from 'firebase/app';
import Company from "@/models/Company";
import User from "@/models/User";
import Router from '../router';
import Task from "@/models/Task";

Vue.use(Vuex);

import ModalModule from './modals/store';
import TemplateModule from './template/store';
import InterviewGuides from './interviewGuide/store';
import PortalModule from './portals/store';
import LangdingPage from "./landingPage/store"
import QuestionModule from './Questionnaires/store';
import PositionPipeline from './position_pipeline/store';
import Invite from "@/models/Invite";
import ScheduleInterviews from "./ScheduleInterviews";
import Scorecard from "@/models/Scorecard";
import Tasks from  "./task/store";
import Candidate from "@/models/Candidate";

import Position from './position/store';

export const store = new Vuex.Store<any>({ // any for test
    modules: {
        modals: ModalModule,
        questionnaire: QuestionModule,
        portal: PortalModule,
        position_pipeline: PositionPipeline,
        templates: TemplateModule,
        schedule_interviews: ScheduleInterviews,
        interview_guide: InterviewGuides,
        position: Position,
        landingPage: LangdingPage,
        tasks: Tasks,
    },
    state: {
        user: null as User | null,
        users: [] as User[] | [],
        company: null as Company | null,
        task: null as Task | null,
        pending_invitation: [] as Invite[] | [],
        candidates: [] as Candidate[] | [],
        last_route: '/',
        score_cards: [] as Scorecard[] | [],
    },
    getters: {
        currentUser: state => {
            return state.user;
        },
        companyUsers: state => {
            return state.users;
        },
        getTask: state => {
            if (state.task == null) {
                state.task = new Task();
            }
            return state.task;
        },
        currentCompany: state => {
            return state.company;
        },
        pendingInvitation: state => {
            return state.pending_invitation;
        },
        currentScorecard: state => {
            return state.score_cards;
        },
        currentCandidates: state => {
            return state.candidates;
        }

    },
    mutations: {
        updateCurrentTask(state, task) {
            state.task = task;
            state.modals.addTask = true;
        },
        setUser(state, user) {
            state.user = user;
        },
        setCompanyUser(state, data) {
            const userIndex = state.users.findIndex(val => val.id === data.id);
            state.users[userIndex] = data;
        },
        setUsers(state, users) {
            state.users = users;
        },
        setCompany(state, company) {
            state.company = company;
        },
        clearCurrentTask(state) {
            state.task = new Task();
        },
        setPendingInvite(state, invites) {
            state.pending_invitation = invites;
        },
        setScorecard(state, scorecards) {
            state.score_cards = scorecards;
        },
        setCandidates(state, candidates) {
            state.candidates = candidates;
        }
    },
    actions: {
        setUser: context => {
            return new Promise((resolve) => {
                firebase.auth().onAuthStateChanged(async fuser => {
                    if (fuser) {
                        let user = await new User().getById(fuser.uid) as User;
                        let company = await new Company().getById(user.company_id) as Company;
                        // let users = await new User().getAllUsers(user.company_id) as User[];
                        if(company.slug == '') company.update()
                        Company.listenRealtime();
                        Company.onAdded((company) => {});
                        Company.onUpdated((company) => {
                            if (company.id == context.state.company.id) {
                                context.state.company = company
                            }
                        });
                        Company.onDeleted((company) => {});

                        User.listenRealtime(user.company_id);
                        User.onAdded((_user) => {
                            const userInStatePos = context.state.users.findIndex(u => u.id === _user.id);
                            if (userInStatePos === -1) {
                                if ((_user.company_id === company.id) && (_user.status != "0"))
                                    context.state.users.push(_user);
                            }
                        });
                        User.onUpdated((_user) => {
                            console.debug('on update', _user)
                            const userInState = context.state.users.find(u => u.id === _user.id);
                            if ((_user.company_id !== user.company_id) || (_user.status == "0")) {
                                const userInStatePos = context.state.users.findIndex(u => u.id === _user.id);
                                if (userInStatePos !== -1)
                                    context.state.users.splice(userInStatePos, 1);
                            }
                            if (userInState) {
                                Object.assign(userInState, _user);
                            } else {
                                context.state.users.push(_user);
                            }
                        });
                        User.onDeleted((user) => {
                            const userInStatePos = context.state.users.findIndex(u => u.id === user.id);
                            if (userInStatePos !== -1)
                                context.state.users.splice(userInStatePos, 1);
                        });

                        context.commit("setUser", user);
                        context.commit("setCompany", company);
                        // context.commit("setUsers", users);
                    } else {
                        Router.push('/login')
                    }
                    resolve();
                });
            });
        },
        getInvitations: async context => {
            let fuser = firebase.auth().currentUser;
            context.state.pending_invitation = [];

            if (fuser) {
                let user = await new User().getById(fuser.uid) as User;

                Invite.listenRealtime(user.company_id);

                console.log(context.state.pending_invitation);

                Invite.onAdded((_invite) => {
                    console.debug('invite on insert', _invite)
                    const inviteInStatePos = context.state.pending_invitation.findIndex(u => u.id === _invite.id);
                    if (inviteInStatePos === -1) {
                        if (_invite.company_id == user.company_id)
                            context.state.pending_invitation.push(_invite);
                    }
                });

                Invite.onUpdated((_invite) => {
                    console.debug('invite on update', _invite)
                    const inviteInState = context.state.pending_invitation.find(u => u.id === _invite.id);
                    if (_invite.company_id !== user.company_id) {
                        const inviteInStatePos = context.state.pending_invitation.findIndex(u => u.id === _invite.id);
                        if (inviteInStatePos !== -1)
                            context.state.pending_invitation.splice(inviteInStatePos, 1);
                    }
                    if (inviteInState) {
                        Object.assign(inviteInState, _invite);
                    } else {
                        context.state.pending_invitation.push(_invite);
                    }
                });

                Invite.onDeleted((_invite) => {
                    console.log(context.state.pending_invitation);
                    console.log("*************");
                    console.debug('invite on delete', _invite);
                    const inviteInStatePos = context.state.pending_invitation.findIndex(u => u.id === _invite.id);
                    console.log("********" + inviteInStatePos + "********");
                    if ((inviteInStatePos !== -1) && (_invite.company_id == user.company_id))
                        context.state.pending_invitation.splice(inviteInStatePos, 1);
                });


            } else {
                Router.push('/login')
            }

        },
        setCurrentCompany: async (context, data) =>{
            context.commit('setCompany',data)
        },
        getScorecard: async context => {
            let fuser = firebase.auth().currentUser;
            context.state.score_cards = [];

            if (fuser) {
                let user = await new User().getById(fuser.uid) as User;

                Scorecard.listenRealtime(user.company_id);

                console.log(context.state.score_cards);

                Scorecard.onAdded((_scorecard) => {
                    console.debug('scorecard on insert', _scorecard)
                    const scorecardInStatePos = context.state.score_cards.findIndex(u => u.id === _scorecard.id);
                    if (scorecardInStatePos === -1) {
                        if (_scorecard.company_id == user.company_id)
                            context.state.score_cards.push(_scorecard);
                    }
                });

                Scorecard.onUpdated((_scorecard) => {
                    console.debug('scorecard on update', _scorecard)
                    const scorecardInState = context.state.score_cards.find(u=>u.id === _scorecard.id);

                    if (scorecardInState) {
                        Object.assign(scorecardInState, _scorecard);
                    }
                    else {
                        context.state.score_cards.push(_scorecard);
                    }
                });

                Scorecard.onDeleted((_scorecard) => {
                    console.log(context.state.score_cards);
                    console.log("*************");
                    console.debug('scorecard on delete', _scorecard);
                    const scorecardInStatePos = context.state.score_cards.findIndex(u => u.id === _scorecard.id);
                    console.log("********" + scorecardInStatePos + "********");
                    if ((scorecardInStatePos !== -1) && (_scorecard.company_id == user.company_id))
                        context.state.score_cards.splice(scorecardInStatePos, 1);
                });


            } else {
                Router.push('/login')
            }
        },

        getCandidates: async context => {
            let fuser = firebase.auth().currentUser;
            context.state.candidates = [];


            if (fuser) {
                let user = await new User().getById(fuser.uid) as User;

                Candidate.listenRealtime(user.company_id);

                //console.log(context.state.candidates);

                Candidate.onAdded((_candidate) => {
                    //console.debug('Candidate on insert', _candidate)
                    const candidateInStatePos = context.state.candidates.findIndex(u => u.id === _candidate.id);
                    if (candidateInStatePos === -1) {
                        if (_candidate.company_id == user.company_id)
                            context.state.candidates.push(_candidate);
                    }
                });

                Candidate.onUpdated((_candidate) => {
                    //console.debug('Candidate on update', _candidate)
                    const CandidateInState = context.state.candidates.find(u => u.id === _candidate.id);
                    if (_candidate.company_id !== user.company_id) {
                        const candidateInStatePos = context.state.candidates.findIndex(u => u.id === _candidate.id);
                        if (candidateInStatePos !== -1)
                            context.state.candidates.splice(candidateInStatePos, 1);
                    }
                    if (CandidateInState) {
                        Object.assign(CandidateInState, _candidate);
                    } else {
                        context.state.candidates.push(_candidate);
                    }
                });

                Candidate.onDeleted(async (_candidate) => {
                    //console.log("*************");
                    //console.log('Candidate on delete ' + _candidate.id);
                    const candidateInStatePos = context.state.candidates.findIndex(u => u.id === _candidate.id);
                    console.log("********" + candidateInStatePos + "********");
                    if ((candidateInStatePos !== -1) && (_candidate.company_id == user.company_id)) {
                        context.state.candidates.splice(candidateInStatePos, 1);
                        const indexID =  context.state.user.candidate_star.findIndex(u => u.id === _candidate.id);
                        if (indexID !== -1) {
                            context.state.user.candidate_star.splice(indexID, 1);

                        }
                    }
                });
            }
        },
        async getUser(context, uid: string): Promise<User | undefined> {
            const users = context.getters.companyUsers as User[];
            return users.find(u=>{
                return u.id === uid;
            });
        },
    },
});
